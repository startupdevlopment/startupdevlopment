package com.gravityacademy.app;

import android.app.Application;
import android.content.Context;

import com.gravityacademy.R;
import com.gravityacademy.retrofit.DataHandler;

import java.util.ArrayList;

/**
 * Created by 05/09/19
 */
public class AppController extends Application {

    public ArrayList<String> getCategories() {
        return categories;
    }

    public void setCategories(ArrayList<String> categories) {
        this.categories = categories;
    }

    private ArrayList<String> categories;

    @Override
    public void onCreate() {
        super.onCreate();
        new DataHandler(this);
        prepareCategories();

        /*try {
            ProviderInstaller.installIfNeeded(getApplicationContext());
        } catch (GooglePlayServicesRepairableException e) {
            e.printStackTrace();
        } catch (GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }*/
    }
    private void prepareCategories(){
        categories = new ArrayList<>();
        categories.add(getString(R.string.muscle));
        categories.add(getString(R.string.cardio));
        categories.add(getString(R.string.strength));
        categories.add(getString(R.string.technique));
    }


}
