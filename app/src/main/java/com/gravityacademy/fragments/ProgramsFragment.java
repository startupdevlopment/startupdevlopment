package com.gravityacademy.fragments;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.gravityacademy.EBookDetailsActivity;
import com.gravityacademy.R;
import com.gravityacademy.activities.AcademyActivity;
import com.gravityacademy.adapters.EbookCourseAdapter;
import com.gravityacademy.model.CategoryModel;

import java.util.ArrayList;


public class ProgramsFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private RadioGroup rgEbook;
    private RadioButton rbHandStand;
    private RadioButton rbLiegestutze;
    private RadioButton rbPlanch;
    private OnProgramsFragmentInteractionListener mListener;
    public ProgramsFragment() {
        // Required empty public constructor
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_programs, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViewIds(view);
    }

    private void resetButtons(){
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                rbHandStand.setChecked(false);
                rbLiegestutze.setChecked(false);
                rbPlanch.setChecked(false);
            }
        },100);

    }
    private void initViewIds(View view) {
        rgEbook = view.findViewById(R.id.rgEbook);
        rbHandStand = view.findViewById(R.id.rbHandStand);
        rbLiegestutze = view.findViewById(R.id.rbLiegestutze);
        rbPlanch = view.findViewById(R.id.rbPlanch);
        rgEbook.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // find which radio button is selected
                Intent intent = null;
                if (checkedId == R.id.rbHandStand) {
                    intent = new Intent(getContext(), AcademyActivity.class);
                    startActivity(intent);
                } else if (checkedId == R.id.rbLiegestutze) {
                    intent = new Intent(getContext(), EBookDetailsActivity.class);
                    startActivity(intent);
                } else {
                    intent = new Intent(getContext(), EBookDetailsActivity.class);
                    startActivity(intent);
                }
               // resetButtons();
            }

        });
    }


    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnProgramsFragmentInteractionListener) {
            mListener = (OnProgramsFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnHomeFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnProgramsFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
