package com.gravityacademy.ProgressView;

public interface OnProgressTrackListener {
    public void onProgressFinish();

    public void onProgressUpdate(int progress);
}

