
package com.gravityacademy.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class WorkoutOfTheDayDataModel implements Serializable {


    @SerializedName("wo_of_the_day")
    @Expose
    private List<Workout> workouts = null;

    public List<Workout> getWorkouts() {
        return workouts;
    }

    public void setWorkouts(List<Workout> workouts) {
        this.workouts = workouts;
    }
}
