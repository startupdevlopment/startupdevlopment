
package com.gravityacademy.model.response;

import java.io.Serializable;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MasterData implements Serializable {

    @SerializedName("fitness_level")
    @Expose
    private List<FitnessLevel> fitnessLevel = null;
    @SerializedName("goals")
    @Expose
    private List<Goal> goals = null;
    @SerializedName("muscle_group")
    @Expose
    private List<MuscleGroup> muscleGroup = null;

    public List<FitnessLevel> getFitnessLevel() {
        return fitnessLevel;
    }

    public void setFitnessLevel(List<FitnessLevel> fitnessLevel) {
        this.fitnessLevel = fitnessLevel;
    }

    public List<Goal> getGoals() {
        return goals;
    }

    public void setGoals(List<Goal> goals) {
        this.goals = goals;
    }

    public List<MuscleGroup> getMuscleGroup() {
        return muscleGroup;
    }

    public void setMuscleGroup(List<MuscleGroup> muscleGroup) {
        this.muscleGroup = muscleGroup;
    }

}
