
package com.gravityacademy.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class MuscleGroup implements Serializable {

    @SerializedName("muscle_id")
    @Expose
    private String muscleId;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("status")
    @Expose
    private String status;

    public String getMuscleId() {
        return muscleId;
    }

    public void setMuscleId(String muscleId) {
        this.muscleId = muscleId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
