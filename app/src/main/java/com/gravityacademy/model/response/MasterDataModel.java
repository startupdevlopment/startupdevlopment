
package com.gravityacademy.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class MasterDataModel extends CommonDataResponseModel implements Serializable {

    @SerializedName("data")
    @Expose
    private MasterDataDataModel data;

    public MasterDataDataModel getData() {
        return data;
    }

    public void setData(MasterDataDataModel data) {
        this.data = data;
    }

}
