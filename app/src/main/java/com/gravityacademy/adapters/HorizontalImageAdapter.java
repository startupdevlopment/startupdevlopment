package com.gravityacademy.adapters;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.constraint.Constraints;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.ScaleAnimation;
import android.widget.TextView;

import com.gravityacademy.R;
import com.gravityacademy.fragments.EBookDetailsFragment;
import com.gravityacademy.fragments.MoreEBookFragment;
import com.gravityacademy.model.CategoryModel;

import java.util.List;
import java.util.Random;

public class HorizontalImageAdapter extends RecyclerView.Adapter<HorizontalImageAdapter.ViewHolder> {

    private Context mContext;
    private List<CategoryModel> categoryModels;
    private EBookDetailsFragment.OnFragmentInteractionListener mListener;
    public HorizontalImageAdapter(Activity mContext, List<CategoryModel> categoryModels, EBookDetailsFragment.OnFragmentInteractionListener mListener) {
        this.mContext = mContext;
        this.categoryModels = categoryModels;
        this.mListener = mListener;
    }

    private int lastPosition = -1;

    public HorizontalImageAdapter(Activity mContext, List<CategoryModel> categoryModels) {
        this.mContext = mContext;
        this.categoryModels = categoryModels;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View mView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_more_academy_book, parent, false);
        if(mListener!=null) {
            mView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mListener.onFragmentInteraction(new MoreEBookFragment());
                }
            });
        }
        return new ViewHolder(mView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
      //  setAnimation(viewHolder.parentLayout, i);
        if (categoryModels.size() > 0 && categoryModels.get(i) != null) {
            final CategoryModel categoryModel = categoryModels.get(i);
            viewHolder.tvTitle.setText(categoryModel.getTitle());

            viewHolder.tvTitle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //Intent intent = new Intent(mContext, SeeAllActivity.class);
                    //intent.putExtra(AppConstant.KEY_TYPE, categoryModel.getType());
                    //mContext.startActivity(intent);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
       // return categoryModels.size();
        return 5;
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        TextView tvTitle;
        ConstraintLayout parentLayout;

        ViewHolder(View itemView) {
            super(itemView);
            tvTitle = itemView.findViewById(R.id.tvTitle);
            parentLayout = itemView.findViewById(R.id.parentLayout);

        }
    }

    /**
     * Here is the key method to apply the animation
     */
    private void setAnimation(View viewToAnimate, int position) {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition) {
            Animation animation = AnimationUtils.loadAnimation(mContext, android.R.anim.slide_in_left);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }

    private void setScaleAnimation(View viewToAnimate, int position) {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition) {
            ScaleAnimation anim = new ScaleAnimation(0.0f, 1.0f, 0.0f, 1.0f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
            anim.setDuration(new Random().nextInt(501));//to make duration random number between [0,501)
            viewToAnimate.startAnimation(anim);
            lastPosition = position;
        }
    }

   /* @Override
    public void onViewAttachedToWindow(@NonNull ViewHolder holder) {
        super.onViewAttachedToWindow(holder);
        ((CustomViewHolder)holder).clearAnimation();
    }

    public void clearAnimation()
    {
        mRootLayout.clearAnimation();
    }*/
}
