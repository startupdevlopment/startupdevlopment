package com.gravityacademy.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gravityacademy.R;
import com.gravityacademy.activities.ProgramScreenActivity;
import com.gravityacademy.model.response.Exercise;
import com.gravityacademy.model.response.Workout;

import java.util.List;

public class FavouriteHorizontalAdapter extends RecyclerView.Adapter<FavouriteHorizontalAdapter.ViewHolder> {

    private Context mContext;
    private List<Workout> workouts;

    public FavouriteHorizontalAdapter(Context mContext, List<Workout> categoryModels) {
        this.mContext = mContext;
        this.workouts = categoryModels;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View mView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_parts_from_workout_item, parent, false);
        return new ViewHolder(mView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        Workout workout = workouts.get(i);
        viewHolder.tvTitle.setText(workout.getTitle());
        viewHolder.parentLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, ProgramScreenActivity.class);
                mContext.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return workouts.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvTitle;
        TextView tvCategory;
        ImageView ivBackground;
        LinearLayout parentLayout;

        ViewHolder(View itemView) {
            super(itemView);
            parentLayout = itemView.findViewById(R.id.parentLayout);
            tvTitle = itemView.findViewById(R.id.tvTitle);
            ivBackground = itemView.findViewById(R.id.ivBackground);
            tvCategory = itemView.findViewById(R.id.tvCategory);
        }
    }
}
