package com.gravityacademy.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gravityacademy.EBookDetailsActivity;
import com.gravityacademy.R;
import com.gravityacademy.activities.AcademyActivity;
import com.gravityacademy.model.CategoryModel;

import java.util.List;

public class EbookCourseAdapter extends RecyclerView.Adapter<EbookCourseAdapter.ViewHolder> {

    private Context mContext;
    private List<CategoryModel> categoryModels;
    private LinearLayoutManager linearLayoutManager;
    private LinearLayout layoutProgressBar;

    public EbookCourseAdapter(Activity mContext, List<CategoryModel> categoryModels) {
        this.mContext = mContext;
        this.categoryModels = categoryModels;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View mView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_item_ebook_courses, parent, false);
        return new ViewHolder(mView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int i) {
        if (i == 0) {
            viewHolder.ivUnlock.setImageDrawable(mContext.getDrawable(R.mipmap.right_tick));
        }else {
            viewHolder.ivUnlock.setImageDrawable(mContext.getDrawable(R.mipmap.lock_circul));

        }
        viewHolder.checkbox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=null;
                if(i==0){
                     intent = new Intent(mContext, AcademyActivity.class);
                }else {
                     intent = new Intent(mContext, EBookDetailsActivity.class);
                }
                mContext.startActivity(intent);
            }
        });
        viewHolder.checkbox.setButtonDrawable(categoryModels.get(i).getImageId());
    }

    @Override
    public int getItemCount() {
        return categoryModels.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        TextView tvShowAll;
        TextView tvTitle;
        TextView tvSubTitle;
        TextView tvCount;
        TextView tvType;
        ImageView ivCategoryImage;
        ImageView ivUnlock;
        CheckBox checkbox;
        LinearLayout parentLayout;

        ViewHolder(View itemView) {
            super(itemView);
            parentLayout = itemView.findViewById(R.id.parentLayout);
            checkbox = itemView.findViewById(R.id.checkbox);
            ivUnlock = itemView.findViewById(R.id.ivUnlock);
            /*tvTitle = itemView.findViewById(R.id.tvTitle);
            tvSubTitle = itemView.findViewById(R.id.tvSubTitle);
            tvCount = itemView.findViewById(R.id.tvCount);
            tvShowAll = itemView.findViewById(R.id.tvShowAll);
            tvType = itemView.findViewById(R.id.tvType);
            ivCategoryImage = itemView.findViewById(R.id.ivCategoryImage);*/
        }
    }
}
