package com.gravityacademy.adapters;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.gravityacademy.R;
import com.gravityacademy.model.CategoryModel;

import java.util.List;

public class HorizontalAdapter extends RecyclerView.Adapter<HorizontalAdapter.ViewHolder> {

    private Context mContext;
    private List<CategoryModel> categoryModels;

    public HorizontalAdapter(Activity mContext, List<CategoryModel> categoryModels) {
        this.mContext = mContext;
        this.categoryModels = categoryModels;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View mView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_checkbox_item, parent, false);
        return new ViewHolder(mView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        if (categoryModels.get(i) != null) {
            final CategoryModel categoryModel = categoryModels.get(i);
            viewHolder.tvTitle.setText(categoryModel.getTitle());

            viewHolder.tvTitle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //Intent intent = new Intent(mContext, SeeAllActivity.class);
                    //intent.putExtra(AppConstant.KEY_TYPE, categoryModel.getType());
                    //mContext.startActivity(intent);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return categoryModels.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        TextView tvTitle;
        ViewHolder(View itemView) {
            super(itemView);
            tvTitle = itemView.findViewById(R.id.tvTitle);

        }
    }
}
