package com.gravityacademy.utils;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;

import com.gravityacademy.BuildConfig;
import com.gravityacademy.R;


/**
 * @author csuryesh
 */
public class AppPermissionManager {

    public static final int RC_MULTI_PERMISSION = 100;
    public static final int RC_STORAGE_PERMISSION = 101;
    public static final int RC_CAMERA_PERMISSION = 102;
    public static final int RC_LOCATION_PERMISSION = 103;
    public static final int RC_APP_PERMISSION = 104;
    public static final int RC_RECORD_AUDIO_PERMISSION = 105;
    public static final int RC_CALENDER_PERMISSION = 106;
    public static final int RC_READ_PHONE_STATE_PERMISSION = 107;

    public static final int REQUEST_PERMISSION_SETTING = 202;


    public static final String PERMISSION_CALENDER = Manifest.permission.WRITE_CALENDAR;
    public static final String PERMISSION_CAMERA = Manifest.permission.CAMERA;
    public static final String PERMISSION_FINE_LOCATION = Manifest.permission.ACCESS_FINE_LOCATION;
    public static final String PERMISSION_COARSE_LOCATION = Manifest.permission.ACCESS_COARSE_LOCATION;
    public static final String PERMISSION_RECORD_AUDIO = Manifest.permission.RECORD_AUDIO;
    public static final String PERMISSION_READ_PHONE_STATE = Manifest.permission.READ_PHONE_STATE;
    public static final String PERMISSION_STORAGE = Manifest.permission.WRITE_EXTERNAL_STORAGE;

    private Context mContext;
    private Activity mActivity;

    String[] permissionArray = new String[]{PERMISSION_CAMERA, PERMISSION_STORAGE};
    String[] appPermissionArray = new String[]{PERMISSION_CAMERA, PERMISSION_STORAGE, PERMISSION_COARSE_LOCATION, PERMISSION_FINE_LOCATION};

    String[] cameraPermissionArray = new String[]{Manifest.permission.CAMERA,
            Manifest.permission.RECORD_AUDIO,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };

    public AppPermissionManager(Context context, Activity activity) {
        mContext = context;
        mActivity = activity;
    }

    public boolean hasStoragePermission() {
        return ContextCompat.checkSelfPermission(mContext, PERMISSION_STORAGE) == PackageManager.PERMISSION_GRANTED;
    }

    public boolean hasCameraPermission() {
        return ContextCompat.checkSelfPermission(mContext, PERMISSION_CAMERA) == PackageManager.PERMISSION_GRANTED;
    }

    public boolean hasRecordAudioPermission() {
        return ContextCompat.checkSelfPermission(mContext, PERMISSION_RECORD_AUDIO) == PackageManager.PERMISSION_GRANTED;
    }

    public void requestForRecordAudioPermission() {
        ActivityCompat.requestPermissions(mActivity, new String[]{PERMISSION_RECORD_AUDIO}, RC_RECORD_AUDIO_PERMISSION);
    }

    public boolean hasLocationAccessPermission() {
        return ContextCompat.checkSelfPermission(mContext, PERMISSION_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(mContext, PERMISSION_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED;
    }

    public void requestMultiplePermission() {
        ActivityCompat.requestPermissions(mActivity, permissionArray, RC_MULTI_PERMISSION);
    }

    public void requestForCameraPermission() {
        ActivityCompat.requestPermissions(mActivity, new String[]{PERMISSION_CAMERA}, RC_CAMERA_PERMISSION);
    }

    public void requestForStoragePermission() {
        ActivityCompat.requestPermissions(mActivity, new String[]{PERMISSION_STORAGE}, RC_STORAGE_PERMISSION);
    }

    public void requestForStoragePermissionFragment(Fragment fragment) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            fragment.requestPermissions(new String[]{PERMISSION_STORAGE}, RC_STORAGE_PERMISSION);
        }
    }

    public void requestForLocationPermission() {
        ActivityCompat.requestPermissions(mActivity, new String[]{PERMISSION_FINE_LOCATION, PERMISSION_COARSE_LOCATION}, RC_LOCATION_PERMISSION);
    }

    public boolean hasCalenderPermission() {
        return ContextCompat.checkSelfPermission(mContext, PERMISSION_CALENDER) == PackageManager.PERMISSION_GRANTED;
    }

    public void requestForCalenderPermission() {
        ActivityCompat.requestPermissions(mActivity, new String[]{PERMISSION_CALENDER}, RC_CALENDER_PERMISSION);
    }

    public boolean hasPermissions() {

        if (!hasCameraPermission() || !hasStoragePermission() || !hasLocationAccessPermission())
            return false;
        else
            return true;

    }

    public void requestAppPermissions() {
        ActivityCompat.requestPermissions(mActivity, appPermissionArray, RC_APP_PERMISSION);

    }

    public void requestMultiPermission() {
        ActivityCompat.requestPermissions(mActivity, cameraPermissionArray, 10);
    }


    public boolean hasMultiPermission() {


        if (!hasCameraPermission() || !hasRecordAudioPermission() || !hasStoragePermission())
            return false;
        else
            return true;
    }


/*
    public void requestForCameraStoragePermission() {
        ActivityCompat.requestPermissions(mActivity, cameraStoragePermissionArray, RC_CAMERA_STORAGE_PERMISSION);
    }

    public boolean checkCameraAndStoragePermissions() {
        if (!hasCameraPermission() || !hasStoragePermission()) {
            requestForCameraStoragePermission();
        } else {
            return true;
        }
        return false;
    }

*/


    public boolean hasReadPhoneStatePermission() {
        return ContextCompat.checkSelfPermission(mContext, PERMISSION_READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED;
    }

    public void requestForReadPhoneStatePermission() {
        ActivityCompat.requestPermissions(mActivity, new String[]{PERMISSION_READ_PHONE_STATE}, RC_READ_PHONE_STATE_PERMISSION);
    }

    /**
     * Try again to request the permission if user has denied a
     * permission and selected the Don't ask again
     * {@link #isPermissionsRationale(int[], String[])}
     *
     * @param msg
     */

    public void showRationalePopUp(final Activity activity, String msg) {
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(mContext, android.R.style.Theme_Material_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(mContext);
        }
        builder.setTitle("Need Permission")
                .setMessage(msg)
                .setPositiveButton(R.string.text_settings, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        Uri uri = Uri.fromParts("package", mContext.getPackageName(), null);
                        intent.setData(uri);
                        mActivity.startActivityForResult(intent, REQUEST_PERMISSION_SETTING);
                    }
                })
                .setNegativeButton(R.string.text_not_now, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                        dialog.dismiss();
                        activity.finish();
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();


    }


    public void showRationalePopUp(String msg) {
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(mContext, android.R.style.Theme_Material_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(mContext);
        }
        builder.setTitle("Need Permission")
                .setMessage(msg)
                .setPositiveButton(R.string.text_settings, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        Uri uri = Uri.fromParts("package", mContext.getPackageName(), null);
                        intent.setData(uri);
                        mActivity.startActivityForResult(intent, REQUEST_PERMISSION_SETTING);
                    }
                })
                .setNegativeButton(R.string.text_not_now, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                        dialog.dismiss();
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();


    }

    /**
     * To check if user has denied a permission and selected the Don't ask again
     * option in the permission request dialog
     *
     * @param grantResults
     * @param permissions
     * @return
     */
    public boolean isPermissionsRationale(int[] grantResults, String[] permissions) {
        if (permissions != null && permissions.length > 0) {
            for (int i = 0, len = permissions.length; i < len; i++) {
                if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                    boolean isRationale = ActivityCompat.shouldShowRequestPermissionRationale(mActivity, permissions[i]);
                    if (isRationale)
                        return true;
                    if (i == len - 1 || !isRationale)
                        return false;
                }
            }
        }
        return true;
    }


    public String getRationalePermissionExplanation(String permissionFor) {
        return mContext.getString(R.string.allow) + " " + BuildConfig.APP_NAME
                + " " + mContext.getString(R.string.access_to_your) + " " + permissionFor
                + " " + mContext.getString(R.string.tap_settings) + " " + permissionFor + " " + mContext.getString(R.string.on);
    }


    /**
     * To get deny permission name for Camera and Storage common request
     *
     * @param context
     * @param grantResults
     * @return
     */
    public String getDeniedPermissionName(Context context, int[] grantResults) {
        String str = "";
        if (grantResults[0] == PackageManager.PERMISSION_DENIED)
            str = context.getResources().getString(R.string.camera);
        if (grantResults[1] == PackageManager.PERMISSION_DENIED) {
            if (!str.isEmpty())
                str = str + " " + context.getResources().getString(R.string.storage);
            else
                str = context.getResources().getString(R.string.storage);
        }
        return str;
    }
}