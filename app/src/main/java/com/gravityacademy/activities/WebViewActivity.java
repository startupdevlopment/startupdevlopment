package com.gravityacademy.activities;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;

import com.gravityacademy.R;
import com.gravityacademy.utils.AppConstant;

public class WebViewActivity extends AppCompatActivity {
    private LinearLayout layoutProgressBar;
    boolean loadingFinished = true;
    boolean redirect = false;
    private  WebView webView;
    @SuppressLint("SetJavaScriptEnabled")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_webview);
        layoutProgressBar = findViewById(R.id.layoutProgressBar);
        layoutProgressBar.setVisibility(View.VISIBLE);
        webView = findViewById(R.id.webview);
        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webView.setWebViewClient(new ChildWebViewClient());
        webView.loadUrl(AppConstant.Equipment_URL);

    }

    private class ChildWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
            if (!loadingFinished) {
                redirect = true;
            }

            loadingFinished = false;
            layoutProgressBar.setVisibility(View.VISIBLE);
            webView.loadUrl(request.getUrl().toString());
            return true;
        }

        @Override
        public void onPageStarted(
                WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            loadingFinished = false;
            layoutProgressBar.setVisibility(View.VISIBLE);
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            if (!redirect) {
                loadingFinished = true;
                layoutProgressBar.setVisibility(View.GONE);
            } else {
                redirect = false;
            }
        }
    }
}
