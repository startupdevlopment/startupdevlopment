package com.gravityacademy.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Chronometer;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.gravityacademy.R;
import com.gravityacademy.adapters.ExerciseParentAdapter;
import com.gravityacademy.model.response.ExercisesLibraryDataModel;
import com.gravityacademy.model.response.ExercisesLibraryModel;
import com.gravityacademy.model.response.ResultSet;
import com.gravityacademy.model.response.Workout;
import com.gravityacademy.model.response.WorkoutLibraryDataModel;
import com.gravityacademy.model.response.WorkoutLibraryDetailsModel;
import com.gravityacademy.model.response.WorkoutLibraryModel;
import com.gravityacademy.retrofit.APIUtils;
import com.gravityacademy.utils.AppConstant;
import com.gravityacademy.utils.AppUtils;
import com.gravityacademy.utils.Utility;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.gravityacademy.utils.AppConstant.SUCCESS_RESPONSE_CODE;

public class StrengthTrainingActivity extends AppCompatActivity implements View.OnClickListener {

    private RecyclerView rvExerciseParent;
    private LinearLayoutManager mLinearLayoutManager;
    private ExerciseParentAdapter mAdapter1;
    private AppCompatButton btnStart, btnPause;
    private Chronometer simpleChronometer;
    private ImageView ivFavourite;
    private List<ResultSet> resultList ;
    private LinearLayout layoutProgressBar;
    private Workout workout;
    private int fitnessLevelId=1,workoutId=1;
    private TextView tvFitnessLevel,tvTitle;
    private RecyclerView rvMuscleGroup;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_strength_training);
        init();
        setupRecyclerView();
        performLikeDislike();
        callWorkoutDetailsAPI();
        getWorkoutObject();
    }

    private void getWorkoutObject(){
        if(getIntent()!=null && getIntent().getSerializableExtra(AppConstant.WORKOUT_OBJ)!=null) {
            workout = (Workout) getIntent().getSerializableExtra(AppConstant.WORKOUT_OBJ);
            fitnessLevelId=workout.getFitnessLevelId();
            workoutId=workout.getWorkoutId();
            setupView(workout);
        }
    }

    private void setupView(Workout workout){
        tvFitnessLevel.setText(AppUtils.getFitnessLevelById(StrengthTrainingActivity.this,fitnessLevelId));
        tvTitle.setText(workout.getTitle());
        AppUtils.setupRecyclerViewMuscleGroup(workout.getMuscleGroups(),rvMuscleGroup,StrengthTrainingActivity.this,R.layout.item_muscle_group_white);

    }


    private void init() {
        rvExerciseParent = findViewById(R.id.rvExerciseParent);
        btnStart = findViewById(R.id.btnStart);
        btnPause = findViewById(R.id.btnPause);
        tvTitle = findViewById(R.id.tvTitle);
        tvFitnessLevel = findViewById(R.id.tvFitnessLevel);
        layoutProgressBar = findViewById(R.id.layoutProgressBar);
        rvMuscleGroup = findViewById(R.id.rvMuscleGroup);
        findViewById(R.id.ivBack).setOnClickListener(this);
        simpleChronometer = findViewById(R.id.chronometer);
        btnStart.setOnClickListener(this);
        btnPause.setOnClickListener(this);
        resultList = new ArrayList<>();
    }


    private void setupRecyclerView() {
        mLinearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        rvExerciseParent.setLayoutManager(mLinearLayoutManager);
            mAdapter1 = new ExerciseParentAdapter(StrengthTrainingActivity.this, resultList);
            rvExerciseParent.setAdapter(mAdapter1);
            rvExerciseParent.setNestedScrollingEnabled(false);
            rvExerciseParent.setHasFixedSize(false);

    }


    public void showBottomSheet(View view) {
        startActivity(new Intent(this, CompleteWorkoutActivity.class));
      /*  FilterBottomSheetFragment addPhotoBottomDialogFragment =
                FilterBottomSheetFragment.newInstance();
        addPhotoBottomDialogFragment.show(getSupportFragmentManager(),
                FilterBottomSheetFragment.TAG);*/
    }


    @Override
    public void onClick(View v) {


        switch (v.getId()) {
            case R.id.btnStart:
                btnStart.setVisibility(View.GONE);
                simpleChronometer.start();
                break;
            case R.id.btnPause:
                if (btnPause.getText().toString().equals(getString(R.string.pause))) {
                    simpleChronometer.stop();
                    btnPause.setText(R.string.resume);
                    btnPause.setCompoundDrawables(getDrawable(R.mipmap.play), null, null, null);
                } else {
                    simpleChronometer.start();
                    btnPause.setText(R.string.pause);
                    btnPause.setCompoundDrawables(getDrawable(R.mipmap.pause_small), null, null, null);
                }
                break;

            case R.id.ivBack:
                onBackPressed();
                break;
        }
    }

    private void performLikeDislike() {
        final ImageView ivFavourite = findViewById(R.id.ivFavourite);
        ivFavourite.setTag("false");
        ivFavourite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ivFavourite.getTag().equals("true")) {
                    ivFavourite.setImageResource(R.mipmap.heart_blank);
                    ivFavourite.setTag("false");
                } else {
                    ivFavourite.setImageResource(R.mipmap.heart_fill);
                    ivFavourite.setTag("true");
                }
            }
        });

    }

    private void callWorkoutDetailsAPI() {
        layoutProgressBar.setVisibility(View.VISIBLE);
        APIUtils.getAPIServiceJson().getWorkoutDetails(workoutId, fitnessLevelId).enqueue(new Callback<ExercisesLibraryModel>() {
            @Override
            public void onResponse(Call<ExercisesLibraryModel> call, Response<ExercisesLibraryModel> response) {
                if (!StrengthTrainingActivity.this.isFinishing()) {
                    layoutProgressBar.setVisibility(View.GONE);
                    if (response != null && response.body() != null) {
                        if (response.body().getResponseCode() == SUCCESS_RESPONSE_CODE) {
                            Gson gson = new Gson();
                            ExercisesLibraryModel obj = gson.fromJson(AppUtils.loadJSONFromAsset(StrengthTrainingActivity.this, "workout_detail.json"), ExercisesLibraryModel.class);
                            ExercisesLibraryDataModel data = obj.getData();
                            resultList  = data.getResultSets();
                            //mAdapter1.notifyDataSetChanged();
                            setupRecyclerView();
                        } else {
                            Utility.showToast(StrengthTrainingActivity.this, R.string.something_went_wrong);
                        }
                    } else if (response != null && response.errorBody() != null) {
                        Utility.showToastString(StrengthTrainingActivity.this, AppUtils.getError(response));
                    } else {
                        Utility.showToast(StrengthTrainingActivity.this, R.string.something_went_wrong);
                    }
                }

            }

            @Override
            public void onFailure(Call<ExercisesLibraryModel> call, Throwable t) {
                if (!StrengthTrainingActivity.this.isFinishing()) {
                    layoutProgressBar.setVisibility(View.GONE);
                    Utility.showToast(StrengthTrainingActivity.this, R.string.something_went_wrong);
                }
            }
        });
    }

}
