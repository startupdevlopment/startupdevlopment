package com.gravityacademy.activities;

import android.content.Intent;
import android.media.Image;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RadioButton;

import com.gravityacademy.R;
import com.gravityacademy.model.request.SignUpData;
import com.gravityacademy.utils.AppConstant;
import com.super_rabbit.wheel_picker.WheelPicker;

import static com.gravityacademy.utils.AppConstant.KEY_IS_FROM_CREATE_AC;
import static com.gravityacademy.utils.AppConstant.KEY_SIGN_UP_DATA;

public class HeightActivity extends AppCompatActivity implements View.OnClickListener {

    ImageView ivClose;
    Button btnSave;
    RadioButton centimeter_rb;
    RadioButton inch_rb;
    WheelPicker numberPicker;
    ProgressBar progress;
    private SignUpData signUpData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_height);
        initView();
        fromCreateAccount();
        getSignUpData();
    }

    private void getSignUpData() {
        if (getIntent() != null && getIntent().getSerializableExtra(KEY_SIGN_UP_DATA) != null) {
            signUpData = (SignUpData) getIntent().getSerializableExtra(KEY_SIGN_UP_DATA);
            signUpData.setHeight_unit("1");
        }
    }

    private void initView() {
        progress = findViewById(R.id.progress);
        btnSave = findViewById(R.id.btnSave);
        ivClose = findViewById(R.id.ivClose);
        numberPicker = findViewById(R.id.numberPicker);
        inch_rb = findViewById(R.id.inch_rb);
        centimeter_rb = findViewById(R.id.centimeter_rb);
        centimeter_rb.setChecked(true);

        if (getIntent() != null && getIntent().getStringExtra(AppConstant.HEIGHT) != null) {
            progress.setVisibility(View.GONE);
            String height = getIntent().getStringExtra(AppConstant.HEIGHT);
            ivClose.setImageResource(R.mipmap.close);
            String[] splitStr = height.split(" ");
            String unit = splitStr[1];
            if (unit.equalsIgnoreCase(getString(R.string.centimeter))) {
                centimeter_rb.setChecked(true);
            } else {
                inch_rb.setChecked(true);
            }
            resetData();
            numberPicker.scrollToValue(splitStr[0]);
        }

        ivClose.setOnClickListener(this);
        centimeter_rb.setOnClickListener(this);
        inch_rb.setOnClickListener(this);
        btnSave.setOnClickListener(this);
    }


    private void resetData() {
        if (centimeter_rb.isChecked()) {
            numberPicker.setMin(132);
            numberPicker.setMax(228);
            numberPicker.invalidate();
        } else {
            numberPicker.setMin(50);
            numberPicker.setMax(90);
            numberPicker.invalidate();
        }
    }

    private void fromCreateAccount() {
        ProgressBar progress = findViewById(R.id.progress);
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            boolean isFromCreateAc = bundle.getBoolean(KEY_IS_FROM_CREATE_AC);
            if (isFromCreateAc) {
                ivClose.setImageResource(R.mipmap.back);
                progress.setVisibility(View.VISIBLE);
                btnSave.setText(getString(R.string.next));
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.centimeter_rb:
                signUpData.setHeight_unit("1");
                numberPicker.setMin(132);
                numberPicker.setMax(228);
                numberPicker.invalidate();
                break;
            case R.id.inch_rb:
                signUpData.setHeight_unit("2");
                numberPicker.setMin(50);
                numberPicker.setMax(90);
                numberPicker.invalidate();
                break;
            case R.id.ivClose:
                finish();
                break;
            case R.id.btnSave:

                if (btnSave.getText().toString().equalsIgnoreCase(getString(R.string.save))) {
                    Intent intent = new Intent();
                    intent.putExtra(AppConstant.HEIGHT, numberPicker.getCurrentItem() + " " + getUnit());
                    setResult(RESULT_OK, intent);
                    finish();
                } else {
                    signUpData.setHeight(numberPicker.getCurrentItem());
                    if (centimeter_rb.isChecked())
                        signUpData.setHeight_unit("1");
                    else
                        signUpData.setHeight_unit("2");
                    Intent intent = new Intent(this, WeightActivity.class);
                    intent.putExtra(KEY_IS_FROM_CREATE_AC, true);
                    intent.putExtra(KEY_SIGN_UP_DATA, signUpData);
                    startActivity(intent);
                }
                break;
        }
    }

    private String getUnit() {
        String unit = "";
        if (centimeter_rb.isChecked())
            unit = centimeter_rb.getText().toString();
        else
            unit = inch_rb.getText().toString();
        return unit;
    }
}
