package com.gravityacademy.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import com.google.gson.Gson;
import com.gravityacademy.R;
import com.gravityacademy.adapters.WorkoutLibraryVerticalAdapter;
import com.gravityacademy.model.response.WorkoutLibraryDetailsModel;
import com.gravityacademy.model.response.WorkoutLibraryDataModel;
import com.gravityacademy.model.response.WorkoutLibraryModel;
import com.gravityacademy.retrofit.APIUtils;
import com.gravityacademy.utils.AppUtils;
import com.gravityacademy.utils.Utility;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.gravityacademy.utils.AppConstant.SUCCESS_RESPONSE_CODE;

public class WorkoutLibraryActivity extends AppCompatActivity {

    private RecyclerView rvWorkoutLibraryVertical;
    private LinearLayoutManager mLinearLayoutManager;
    private WorkoutLibraryVerticalAdapter mAdapter;
    private List<WorkoutLibraryDetailsModel> workoutLibraryDetailsModel = new ArrayList<>();
    private LinearLayout layoutProgressBar;
    private Button btnIntro;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_workout_library);
        rvWorkoutLibraryVertical = findViewById(R.id.rvWorkoutLibraryVertical);
        layoutProgressBar = findViewById(R.id.layoutProgressBar);
        btnIntro = findViewById(R.id.btnIntro);
        btnIntro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               startActivity(new Intent(WorkoutLibraryActivity.this,IntroVideoActivity.class));
            }
        });
        findViewById(R.id.ivBack).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        setupRecyclerView();
        callExerciseDataAPI();
    }

    private void setupRecyclerView() {
        mLinearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        rvWorkoutLibraryVertical.setLayoutManager(mLinearLayoutManager);
        mAdapter = new WorkoutLibraryVerticalAdapter(this, workoutLibraryDetailsModel);
        rvWorkoutLibraryVertical.setAdapter(mAdapter);
        rvWorkoutLibraryVertical.setNestedScrollingEnabled(false);
    }

    private void prepairList() {
        Gson gson = new Gson();
        WorkoutLibraryModel obj = gson.fromJson(AppUtils.loadJSONFromAsset(WorkoutLibraryActivity.this, "workout_library.json"), WorkoutLibraryModel.class);
        workoutLibraryDetailsModel = obj.getData().getWorkoutLibraryDetailsModel();
    }

    private void callExerciseDataAPI() {
        layoutProgressBar.setVisibility(View.VISIBLE);
        APIUtils.getAPIServiceJson().getWorkoutLibrary().enqueue(new Callback<WorkoutLibraryModel>() {
            @Override
            public void onResponse(Call<WorkoutLibraryModel> call, Response<WorkoutLibraryModel> response) {
                if (!WorkoutLibraryActivity.this.isFinishing()) {
                    layoutProgressBar.setVisibility(View.GONE);
                    if (response != null && response.body() != null) {
                        if (response.body().getResponseCode() == SUCCESS_RESPONSE_CODE) {
                            WorkoutLibraryDataModel workoutLibraryDataModel = response.body().getData();
                            removeItemWithEmptyData(workoutLibraryDataModel.getWorkoutLibraryDetailsModel());
                           // setupRecyclerView();
                            // mAdapter.notifyDataSetChanged();
                        } else {
                            Utility.showToast(WorkoutLibraryActivity.this, R.string.something_went_wrong);
                        }
                    } else if (response != null && response.errorBody() != null) {
                        Utility.showToastString(WorkoutLibraryActivity.this, AppUtils.getError(response));
                    } else {
                        Utility.showToast(WorkoutLibraryActivity.this, R.string.something_went_wrong);
                    }
                }

            }

            @Override
            public void onFailure(Call<WorkoutLibraryModel> call, Throwable t) {
                if (!WorkoutLibraryActivity.this.isFinishing()) {
                    layoutProgressBar.setVisibility(View.GONE);
                    Utility.showToast(WorkoutLibraryActivity.this, R.string.something_went_wrong);
                }
            }
        });
    }

    private void removeItemWithEmptyData(List<WorkoutLibraryDetailsModel> workoutLibraryDetailsModels) {
        List<WorkoutLibraryDetailsModel> resultSetsDummy = new ArrayList<>();
        for (WorkoutLibraryDetailsModel workoutLibraryDetailsModel : workoutLibraryDetailsModels) {
            if (workoutLibraryDetailsModel.getWorkouts().size() > 0) {
                resultSetsDummy.add(workoutLibraryDetailsModel);
            }
        }
        workoutLibraryDetailsModel.clear();
        workoutLibraryDetailsModel.addAll(resultSetsDummy);
        mAdapter.notifyDataSetChanged();
    }


}
