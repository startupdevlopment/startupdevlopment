package com.gravityacademy.activities;

import android.content.Intent;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.gravityacademy.R;
import com.gravityacademy.model.request.SignUpData;
import com.gravityacademy.utils.AppConstant;
import com.gravityacademy.utils.AppUtils;
import com.gravityacademy.utils.ViewUtils;

import static com.gravityacademy.utils.AppConstant.KEY_IS_FROM_CREATE_AC;
import static com.gravityacademy.utils.AppConstant.KEY_SIGN_UP_DATA;
import static com.gravityacademy.utils.AppConstant.MAX_DIPS;
import static com.gravityacademy.utils.AppConstant.MAX_PULL_UPS;
import static com.gravityacademy.utils.AppConstant.MAX_PUSH_UPS;
import static com.gravityacademy.utils.AppConstant.MAX_SQUATS;

public class PerformanceActivity extends AppCompatActivity implements View.OnClickListener {

    ImageView ivClose;
    Button btnSave;
    private EditText etMaxPullups;
    private EditText etMaxPushups;
    private EditText etMaxSquats;
    private EditText etMaxDips;

    private CoordinatorLayout coordinatorLayout;
    ProgressBar progress;
    private SignUpData signUpData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_performance);
        initView();
        getBundleData();
        fromCreateAccount();
        getSignUpData();
    }
    private void getSignUpData() {
        if (getIntent() != null && getIntent().getSerializableExtra(KEY_SIGN_UP_DATA) != null) {
            signUpData = (SignUpData) getIntent().getSerializableExtra(KEY_SIGN_UP_DATA);
        }
    }
    private void getBundleData(){
        if (getIntent() != null){
            progress.setVisibility(View.GONE);
            ivClose.setImageResource(R.mipmap.close);
            if (getIntent().getStringExtra(AppConstant.MAX_PULL_UPS) != null){
                String str = getIntent().getStringExtra(AppConstant.MAX_PULL_UPS);
                etMaxPullups.setText(str);
            }
            if (getIntent() != null &&
                    (getIntent().getStringExtra(AppConstant.MAX_PUSH_UPS) != null)){
                String str = getIntent().getStringExtra(AppConstant.MAX_PUSH_UPS);
                etMaxPushups.setText(str);
            }
            if (getIntent() != null &&
                    (getIntent().getStringExtra(AppConstant.MAX_SQUATS) != null)){
                String str = getIntent().getStringExtra(AppConstant.MAX_SQUATS);
                etMaxSquats.setText(str);
            }
            if (getIntent() != null &&
                    (getIntent().getStringExtra(AppConstant.MAX_DIPS) != null)){
                String str = getIntent().getStringExtra(AppConstant.MAX_DIPS);
                etMaxDips.setText(str);
            }
        }
    }
    private void initView(){
        progress = findViewById(R.id.progress);
        coordinatorLayout = findViewById(R.id.coordinatorLayout);
        btnSave = findViewById(R.id.btnSave);
        ivClose = findViewById(R.id.ivClose);

        etMaxPullups = findViewById(R.id.etMaxPullups);
        etMaxPushups = findViewById(R.id.etMaxPushups);;
        etMaxSquats = findViewById(R.id.etMaxSquats);;
        etMaxDips = findViewById(R.id.etMaxDips);;

        ivClose.setOnClickListener(this);
        btnSave.setOnClickListener(this);
    }

    private void fromCreateAccount(){
        ProgressBar progress = findViewById(R.id.progress);
        Bundle bundle = getIntent().getExtras();
        if (bundle != null){
            boolean isFromCreateAc = bundle.getBoolean(KEY_IS_FROM_CREATE_AC);
            if (isFromCreateAc){
                ivClose.setImageResource(R.mipmap.back);
                progress.setVisibility(View.VISIBLE);
                btnSave.setText(getString(R.string.next));
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivClose:
                finish();
                break;
            case R.id.btnSave:
                if (!AppUtils.isValidateString(this, etMaxPullups)
                        || !AppUtils.isValidateString(this, etMaxPushups)
                        || !AppUtils.isValidateString(this, etMaxSquats)
                        || !AppUtils.isValidateString(this, etMaxDips)
                ) {
                    Snackbar snackbar = ViewUtils.showCustomSnackBar(this,coordinatorLayout, getString(R.string.fill_in_all_fields));
                    snackbar.show();
                    return;
                }
                if (btnSave.getText().toString().equalsIgnoreCase(getString(R.string.save))) {
                    Intent intent = new Intent();
                    intent.putExtra(MAX_PULL_UPS, etMaxPullups.getText().toString());
                    intent.putExtra(MAX_PUSH_UPS, etMaxPushups.getText().toString());
                    intent.putExtra(MAX_SQUATS, etMaxSquats.getText().toString());
                    intent.putExtra(MAX_DIPS, etMaxDips.getText().toString());
                    setResult(RESULT_OK, intent);
                    finish();
                } else {
                    signUpData.setMax_pullups(Integer.valueOf(etMaxPullups.getText().toString()));
                    signUpData.setMax_pushups(Integer.valueOf(etMaxPushups.getText().toString()));
                    signUpData.setMax_squats(Integer.valueOf(etMaxSquats.getText().toString()));
                    signUpData.setMax_dips(Integer.valueOf(etMaxDips.getText().toString()));
                    Intent intent = new Intent(this, SignUpActivity.class);
                    intent.putExtra(KEY_IS_FROM_CREATE_AC, true);
                    intent.putExtra(KEY_SIGN_UP_DATA, signUpData);
                    startActivity(intent);
                }
                break;
        }
    }
}
