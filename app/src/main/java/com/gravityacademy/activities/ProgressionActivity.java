package com.gravityacademy.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.gravityacademy.R;
import com.gravityacademy.adapters.ProgramAdapter;
import com.gravityacademy.adapters.WatchVideoAdapter;
import com.gravityacademy.model.CategoryModel;

import java.util.ArrayList;

public class ProgressionActivity extends AppCompatActivity {


    Toolbar toolbar;

    private RecyclerView recyclerView;
    private WatchVideoAdapter mAdapter;
    private ArrayList<CategoryModel> categoryModels;
    private LinearLayoutManager mLinearLayoutManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_progression);
        setToolBar();
        initViewIds();
        setupRecyclerView();
    }

    private void setupRecyclerView() {
        mLinearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(mLinearLayoutManager);
        categoryModels = new ArrayList<>();
        prepairList();
        mAdapter = new WatchVideoAdapter(this, categoryModels);
        recyclerView.setAdapter(mAdapter);
        recyclerView.setNestedScrollingEnabled(false);
    }

    private void prepairList() {
        CategoryModel categoryModel = new CategoryModel();
        categoryModel.setType(getString(R.string.workouts));
        categoryModel.setTitle(getString(R.string.workouts));
        categoryModel.setSubTitle(getString(R.string.workouts_archiv));
        categoryModel.setCount(12);
        categoryModel.setImageId(R.mipmap.workout);
        categoryModels.add(categoryModel);
    }

    private void setToolBar(){
        toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("");
        //  toolbar.setBackgroundColor(Color.WHITE);
        toolbar.setNavigationIcon(R.mipmap.back);
        /// toolbar.setBac
        toolbar.setTitleTextColor(getResources().getColor(R.color.black));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void initViewIds(){
        recyclerView = findViewById(R.id.recyclerView);
    }
}
