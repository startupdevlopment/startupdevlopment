package com.gravityacademy.activities;

import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.support.design.widget.BottomSheetDialog;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.gravityacademy.BuildConfig;
import com.gravityacademy.R;
import com.gravityacademy.model.response.CommonResponse;
import com.gravityacademy.retrofit.APIUtils;
import com.gravityacademy.retrofit.DataHandler;
import com.gravityacademy.utils.AppConstant;
import com.gravityacademy.utils.AppPermissionManager;
import com.gravityacademy.utils.AppUtils;
import com.gravityacademy.utils.ImageCompressor;
import com.gravityacademy.utils.Utility;

import java.io.File;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.gravityacademy.utils.AppConstant.CODE_CAMERA;
import static com.gravityacademy.utils.AppConstant.CODE_CAMERA_CROP;
import static com.gravityacademy.utils.AppConstant.CODE_CHANGE_PASSWORD;
import static com.gravityacademy.utils.AppConstant.CODE_GALLERY;
import static com.gravityacademy.utils.AppConstant.CODE_GALLERY_CROP;
import static com.gravityacademy.utils.AppConstant.CODE_GENDER_SELECT;
import static com.gravityacademy.utils.AppConstant.SP_SESSION_KEY;
import static com.gravityacademy.utils.AppConstant.SUCCESS_RESPONSE_CODE;

public class EditProfileActivity extends AppCompatActivity implements View.OnClickListener {


    private ImageView ivProfile;
    private ImageView ivProfileEdit;
    private TextView tvGender;
    private TextView tvHeight;
    private TextView tvWeight;
    private TextView tvFitnessLevel;
    private TextView tvAim;
    private TextView tvMaxPullUps;
    private TextView tvPushUps;
    private TextView tvSquats;
    private TextView tvDips;
    private File cameraProfileImage, cropProfileImage;
    private RelativeLayout layoutGender;
    private RelativeLayout layoutHeight;
    private RelativeLayout layoutWeight;
    private RelativeLayout layoutFitnessLevel;
    private RelativeLayout layoutAim;
    private RelativeLayout layoutMaxPull;
    private RelativeLayout layoutMaxPush;
    private RelativeLayout layoutMaxSquats;
    private RelativeLayout layoutMaxDips;
    private RelativeLayout layoutChangePassword;
    private RelativeLayout layoutMembership;
    private RelativeLayout layoutLogout;
    private RelativeLayout signOutLayout;
    private ImageView ivBack;
    private LinearLayout layoutProgressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        init();
    }

    private void init() {
        layoutProgressBar = findViewById(R.id.layoutProgressBar);
        ivProfile = findViewById(R.id.ivProfile);
        ivProfileEdit = findViewById(R.id.ivProfileEdit);
        layoutGender = findViewById(R.id.layoutGender);
        layoutHeight = findViewById(R.id.layoutHeight);
        layoutChangePassword = findViewById(R.id.layoutChangePassword);
        layoutMembership = findViewById(R.id.layoutMembership);
        ivBack = findViewById(R.id.ivBack);

        tvGender = findViewById(R.id.tvGender);
        tvHeight = findViewById(R.id.tvHeight);
        tvWeight = findViewById(R.id.tvWeight);
        tvFitnessLevel = findViewById(R.id.tvFitnessLevel);
        tvAim = findViewById(R.id.tvAim);
        tvMaxPullUps = findViewById(R.id.tvMaxPullUps);
        tvPushUps = findViewById(R.id.tvPushUps);
        tvSquats = findViewById(R.id.tvSquats);
        tvDips = findViewById(R.id.tvDips);

        layoutWeight = findViewById(R.id.layoutWeight);
        layoutFitnessLevel = findViewById(R.id.layoutFitnessLevel);
        layoutAim = findViewById(R.id.layoutAim);
        layoutMaxPull = findViewById(R.id.layoutMaxPull);
        layoutMaxPush = findViewById(R.id.layoutMaxPush);
        layoutMaxSquats = findViewById(R.id.layoutMaxSquats);
        layoutMaxDips = findViewById(R.id.layoutMaxDips);
        signOutLayout = findViewById(R.id.signOutLayout);

        signOutLayout.setOnClickListener(this);
        ivProfileEdit.setOnClickListener(this);
        layoutGender.setOnClickListener(this);
        layoutHeight.setOnClickListener(this);
        ivBack.setOnClickListener(this);
        layoutChangePassword.setOnClickListener(this);
        layoutMembership.setOnClickListener(this);

        layoutWeight.setOnClickListener(this);
        layoutFitnessLevel.setOnClickListener(this);
        layoutAim.setOnClickListener(this);
        layoutMaxPull.setOnClickListener(this);
        layoutMaxPush.setOnClickListener(this);
        layoutMaxSquats.setOnClickListener(this);
        layoutMaxDips.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.layoutGender:
                Intent intent = new Intent(EditProfileActivity.this, GenderSelectActivity.class);
                intent.putExtra(AppConstant.GENDER, tvGender.getText().toString());
                startActivityForResult(intent, CODE_GENDER_SELECT);
                break;
            case R.id.layoutHeight:
                intent = new Intent(EditProfileActivity.this, HeightActivity.class);
                intent.putExtra(AppConstant.HEIGHT, tvHeight.getText().toString());
                startActivityForResult(intent, CODE_GENDER_SELECT);
                break;
            case R.id.layoutWeight:
                intent = new Intent(EditProfileActivity.this, WeightActivity.class);
                intent.putExtra(AppConstant.WEIGHT, tvWeight.getText().toString());
                startActivityForResult(intent, CODE_GENDER_SELECT);
                break;
            case R.id.layoutFitnessLevel:
                intent = new Intent(EditProfileActivity.this, FitnessLevelActivity.class);
                intent.putExtra(AppConstant.FITNESS_LEVEL, tvFitnessLevel.getText().toString());
                startActivityForResult(intent, CODE_GENDER_SELECT);
                break;
            case R.id.layoutAim:
                intent = new Intent(EditProfileActivity.this, GoalsActivity.class);
                intent.putExtra(AppConstant.AIM, tvAim.getText().toString());
                startActivityForResult(intent, CODE_GENDER_SELECT);
                break;
            case R.id.layoutMaxPull:
            case R.id.layoutMaxPush:
            case R.id.layoutMaxSquats:
            case R.id.layoutMaxDips:
                intent = new Intent(EditProfileActivity.this, PerformanceActivity.class);
                intent.putExtra(AppConstant.MAX_PULL_UPS, tvMaxPullUps.getText().toString());
                intent.putExtra(AppConstant.MAX_PUSH_UPS, tvPushUps.getText().toString());
                intent.putExtra(AppConstant.MAX_SQUATS, tvSquats.getText().toString());
                intent.putExtra(AppConstant.MAX_DIPS, tvDips.getText().toString());
                startActivityForResult(intent, CODE_GENDER_SELECT);
                break;
            case R.id.ivProfileEdit:
                showImageChooserPopUp();
                break;
            case R.id.layoutChangePassword:
                intent = new Intent(EditProfileActivity.this, ChangePasswordActivity.class);
                startActivityForResult(intent, CODE_CHANGE_PASSWORD);
                break;
            case R.id.layoutMembership:
                intent = new Intent(EditProfileActivity.this, MemberShipActivity.class);
                startActivity(intent);
                break;
            case R.id.ivBack:
                onBackPressed();
                break;
            case R.id.signOutLayout:
                showLogOutDialog();
                break;
        }
    }

    private void callLogoutAPI(){
        layoutProgressBar.setVisibility(View.VISIBLE);
        APIUtils.getAPIServiceJson().userLogout(DataHandler.getStringPreferences(SP_SESSION_KEY)
        ).enqueue(new Callback<CommonResponse>() {
            @Override
            public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {
                if (!EditProfileActivity.this.isFinishing()) {
                    layoutProgressBar.setVisibility(View.GONE);
                    if (response != null && response.body() != null) {
                        if (response.body().getResponse_code() == SUCCESS_RESPONSE_CODE) {
                            Utility.showToastString(EditProfileActivity.this, response.body().getMessage());
                            AppUtils.onLogout(EditProfileActivity.this);
                        } else {
                            Utility.showToast(EditProfileActivity.this, R.string.something_went_wrong);
                        }
                    } else if (response != null && response.errorBody() != null){
                        Utility.showToastString(EditProfileActivity.this, AppUtils.getError(response));
                    }else {
                        Utility.showToast(EditProfileActivity.this, R.string.something_went_wrong);
                    }
                }
            }
            @Override
            public void onFailure(Call<CommonResponse> call, Throwable t) {
                if (!EditProfileActivity.this.isFinishing()) {
                    layoutProgressBar.setVisibility(View.GONE);
                    Utility.showToast(EditProfileActivity.this, R.string.something_went_wrong);
                }
            }
        });
    }

    public void showBottomSheetDialog() {
        View view = getLayoutInflater().inflate(R.layout.fragment_bottom_sheet, null);
        final BottomSheetDialog dialog = new BottomSheetDialog(EditProfileActivity.this);
        dialog.setContentView(view);
        TextView tvCamera = view.findViewById(R.id.tvCamera);
        TextView tvGallery = view.findViewById(R.id.tvGallery);
        TextView tvCancel = view.findViewById(R.id.tvCancel);
        tvCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                cameraProfileImage = AppUtils.getOutputMediaFile(EditProfileActivity.this);
                AppUtils.cameraCapture(EditProfileActivity.this, cameraProfileImage, CODE_CAMERA, false);
            }
        });
        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        tvGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                AppUtils.galleryCapture(EditProfileActivity.this, CODE_GALLERY);
            }
        });
        dialog.show();
    }

    public void showImageChooserPopUp() {
        AppPermissionManager manager = new AppPermissionManager(EditProfileActivity.this, EditProfileActivity.this);
        if (manager.hasCameraPermission() && manager.hasStoragePermission()) {
            showBottomSheetDialog();
        } else {
            manager.requestMultiplePermission();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case CODE_CAMERA:
                    cropProfileImage = AppUtils.getOutputMediaFile(Objects.requireNonNull(EditProfileActivity.this));
                    AppUtils.cameraCrop(EditProfileActivity.this, cameraProfileImage, cropProfileImage, CODE_CAMERA_CROP);
                    break;
                case CODE_CAMERA_CROP: {
                    String imagePath = ImageCompressor.compressImage(cropProfileImage.getAbsolutePath(), EditProfileActivity.this, null);
                    AppUtils.setProfileImage(EditProfileActivity.this, imagePath, ivProfile);
                    break;
                }
                case CODE_GALLERY:
                    cropProfileImage = AppUtils.getOutputMediaFile(Objects.requireNonNull(EditProfileActivity.this));
                    AppUtils.galleryCrop(EditProfileActivity.this, data.getData(), cropProfileImage, CODE_GALLERY_CROP);
                    break;

                case CODE_GALLERY_CROP:
                    String imagePath = ImageCompressor.compressImage(cropProfileImage.getAbsolutePath(), EditProfileActivity.this, null);
                    AppUtils.setProfileImage(EditProfileActivity.this, imagePath, ivProfile);
                    break;
                case CODE_GENDER_SELECT:
                    if (data != null && data.getStringExtra(AppConstant.GENDER) != null)
                        tvGender.setText(data.getStringExtra(AppConstant.GENDER));
                    else if (data != null && data.getStringExtra(AppConstant.HEIGHT) != null)
                        tvHeight.setText(data.getStringExtra(AppConstant.HEIGHT));
                    else if (data != null && data.getStringExtra(AppConstant.WEIGHT) != null)
                        tvWeight.setText(data.getStringExtra(AppConstant.WEIGHT));
                    else if (data != null && data.getStringExtra(AppConstant.FITNESS_LEVEL) != null)
                        tvFitnessLevel.setText(data.getStringExtra(AppConstant.FITNESS_LEVEL));
                    else if (data != null && data.getStringExtra(AppConstant.AIM) != null)
                        tvAim.setText(data.getStringExtra(AppConstant.AIM));

                    if (data != null && data.getStringExtra(AppConstant.MAX_PULL_UPS) != null)
                        tvMaxPullUps.setText(data.getStringExtra(AppConstant.MAX_PULL_UPS));
                    if (data != null && data.getStringExtra(AppConstant.MAX_PUSH_UPS) != null)
                        tvPushUps.setText(data.getStringExtra(AppConstant.MAX_PUSH_UPS));
                    if (data != null && data.getStringExtra(AppConstant.MAX_SQUATS) != null)
                        tvSquats.setText(data.getStringExtra(AppConstant.MAX_SQUATS));
                    if (data != null && data.getStringExtra(AppConstant.MAX_DIPS) != null)
                        tvDips.setText(data.getStringExtra(AppConstant.MAX_DIPS));
                    break;

            }
        }
    }


    private void galleryCapture(int code) {
        try {
            startActivityForResult(new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
                    .setType("image/*"), code);
        } catch (ActivityNotFoundException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void cameraCapture(File cameraInput, int code, boolean isFrontCamera) {
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.M) {
            startActivityForResult(new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE)
                    .putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(cameraInput)).putExtra("android.intent.extras.CAMERA_FACING", 1), code);
        } else {
            Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            Uri photoURI = FileProvider.getUriForFile(EditProfileActivity.this, BuildConfig.APPLICATION_ID + ".provider", cameraInput);
            cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
            if (isFrontCamera)
                cameraIntent.putExtra("android.intent.extras.CAMERA_FACING", 1);
            startActivityForResult(cameraIntent, code);
        }
    }

    private void cameraCrop(File cropInput, File cropOutput, int code) {

        try {
            Intent cropIntent = null;
            if (cropInput != null) {
                if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.M) {
                    cropIntent = new Intent("com.android.camera.action.CROP", Uri.fromFile(cropInput));
                    cropIntent.setDataAndType(Uri.fromFile(cropInput), "image/*");
                } else {
                    Uri photoURI = FileProvider.getUriForFile(EditProfileActivity.this, BuildConfig.APPLICATION_ID + ".provider", cropInput);
                    cropIntent = new Intent("com.android.camera.action.CROP", photoURI);
                    cropIntent.putExtra("aspectX", 1);
                    cropIntent.putExtra("aspectY", 1);
                    //indicate output X and Y
                    //cropIntent.putExtra("outputX", 440);
                    //cropIntent.putExtra("outputY", 640);
                    cropIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    cropIntent.setDataAndType(photoURI, "image/*");
                }

                cropIntent.putExtra("crop", "true");
                cropIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(cropOutput));

                startActivityForResult(cropIntent, code);
            } else {
                //Utility.showToast(StartInterviewActivity.this, "Unable to take picture, please try again");
            }
        } catch (ActivityNotFoundException anfe) {
            AppUtils.showToast(EditProfileActivity.this, R.string.txt_no_activity_found_to_handle_intent);
            //Utility.showToastString(activity, anfe.getMessage());
            anfe.printStackTrace();

        }


    }

    private void galleryCrop(Uri cropInput, File cropOutput, int code) {

        try {
            Intent cropIntent = new Intent("com.android.camera.action.CROP", cropInput);
            cropIntent.setDataAndType(cropInput, "image/*");
            cropIntent.putExtra("crop", "true");
            cropIntent.putExtra("aspectX", 1);
            cropIntent.putExtra("aspectY", 1);
            cropIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(cropOutput));
            startActivityForResult(cropIntent, code);
        } catch (ActivityNotFoundException e) {
            AppUtils.showToast(EditProfileActivity.this, R.string.txt_no_activity_found_to_handle_intent);
            e.printStackTrace();
        } catch (Exception e) {
            AppUtils.showToast(EditProfileActivity.this, R.string.txt_error_on_crop);
            e.printStackTrace();
        }
    }

    private void showLogOutDialog() {
        final Dialog dialog = new Dialog(this, R.style.DialogCustomTheme);
        dialog.requestWindowFeature(1);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_logout_alert);
        Button tvNegative = dialog.findViewById(R.id.btnCancel);
        Button tvPositive = dialog.findViewById(R.id.btnOk);
        tvNegative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        tvPositive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                callLogoutAPI();
            }
        });
        dialog.show();
    }
}


