package com.gravityacademy.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.gravityacademy.R;
import com.gravityacademy.adapters.ExerciseChildAdapter;
import com.gravityacademy.fragments.FilterBottomSheetFragment;
import com.gravityacademy.model.response.Exercise;

import java.util.ArrayList;

public class AllExerciseActivity extends AppCompatActivity implements View.OnClickListener {

    private RecyclerView rvExercise;
    private LinearLayoutManager mLinearLayoutManager;
    private ArrayList<Exercise> exersiseList;
    private ExerciseChildAdapter mAdapter;
    private ImageView ivFilter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_exercise);
        rvExercise = findViewById(R.id.rvExercise);
        ivFilter = findViewById(R.id.ivFilter);
       findViewById(R.id.ivBack).setOnClickListener(this);
        ivFilter.setOnClickListener(this);
        prepairList();
        setupRecyclerView();
    }

    private void setupRecyclerView() {
        mLinearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        rvExercise.setLayoutManager(mLinearLayoutManager);
        mAdapter = new ExerciseChildAdapter(this, exersiseList, true);
        rvExercise.setAdapter(mAdapter);

    }

    private void prepairList() {
        exersiseList = new ArrayList<>();
        Exercise categoryModel = new Exercise();
        categoryModel.setTitle(getString(R.string.chest));
        exersiseList.add(categoryModel);

        categoryModel = new Exercise();
        categoryModel.setTitle(getString(R.string.traisap));
        exersiseList.add(categoryModel);

        categoryModel = new Exercise();
        categoryModel.setTitle(getString(R.string.shoulder));
        exersiseList.add(categoryModel);

        categoryModel = new Exercise();
        categoryModel.setTitle(getString(R.string.shoulder));
        exersiseList.add(categoryModel);
        categoryModel = new Exercise();
        categoryModel.setTitle(getString(R.string.shoulder));
        exersiseList.add(categoryModel);
        categoryModel = new Exercise();
        categoryModel.setTitle(getString(R.string.shoulder));
        exersiseList.add(categoryModel);

        categoryModel = new Exercise();
        categoryModel.setTitle(getString(R.string.shoulder));
        exersiseList.add(categoryModel);
        categoryModel = new Exercise();
        categoryModel.setTitle(getString(R.string.shoulder));
        exersiseList.add(categoryModel);

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.ivFilter:
                showBottomSheet();
                break;
            case R.id.ivBack:
                onBackPressed();
                break;

        }
    }

    public void showBottomSheet() {
        FilterBottomSheetFragment addPhotoBottomDialogFragment =
                FilterBottomSheetFragment.newInstance();
        addPhotoBottomDialogFragment.show(getSupportFragmentManager(),
                FilterBottomSheetFragment.TAG);
    }

}
