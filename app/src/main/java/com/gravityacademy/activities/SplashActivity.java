package com.gravityacademy.activities;

import android.content.Intent;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.gravityacademy.R;
import com.gravityacademy.fragments.SplashFragment;
import com.gravityacademy.fragments.StartUpFragment;
import com.gravityacademy.model.response.MasterDataModel;
import com.gravityacademy.retrofit.APIUtils;
import com.gravityacademy.utils.AppUtils;
import com.gravityacademy.utils.Utility;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.gravityacademy.utils.AppConstant.SUCCESS_RESPONSE_CODE;

public class SplashActivity extends AppCompatActivity
        implements SplashFragment.OnFragmentInteractionListener,
        StartUpFragment.OnFragmentInteractionListener {

    FragmentTransaction fragmentTransaction;
    FragmentManager fragmentManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_activitiy);
        callMasterDataAPI();
        /*new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(new Intent(SplashActivity.this, LoginActivity.class));
                finish();
            }
        }, 1000);*/
    }

    private void defaultFragment() {
        // Create and set Android Fragment as default.
        Fragment fragment = new SplashFragment();
        setDefaultFragment(fragment);
    }

    // This method is used to set the default fragment that will be shown.
    private void setDefaultFragment(Fragment defaultFragment) {
        this.replaceFragment(defaultFragment);
    }

    public void replaceFragment(Fragment destFragment) {
        fragmentManager = this.getSupportFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();
        Log.e("destFragment: ", destFragment.getClass().getSimpleName());
        fragmentTransaction.replace(R.id.dynamic_fragment_frame_layout, destFragment, destFragment.getClass().getSimpleName());
        /*if (!(destFragment instanceof SplashFragment))
            fragmentTransaction.addToBackStack(destFragment.getClass().getSimpleName());*/
        fragmentTransaction.commit();
    }

    @Override
    public void onFragmentInteraction(Fragment fragment) {
        replaceFragment(fragment);
    }

    private void callMasterDataAPI(){
        startActivity(new Intent(SplashActivity.this, LoginActivity.class));
        finish();

//        APIUtils.getAPIServiceJson().getMasterData().enqueue(new Callback<MasterDataModel>() {
//            @Override
//            public void onResponse(Call<MasterDataModel> call, Response<MasterDataModel> response) {
//                if (!SplashActivity.this.isFinishing()) {
//                    if (response != null && response.body() != null) {
//                        if (response.body().getResponseCode() == SUCCESS_RESPONSE_CODE) {
//                            startActivity(new Intent(SplashActivity.this, LoginActivity.class));
//                            finish();
//                        } else {
//                            Utility.showToast(SplashActivity.this, R.string.something_went_wrong);
//                        }
//                    } else if (response != null && response.errorBody() != null){
//                        Utility.showToastString(SplashActivity.this, AppUtils.getError(response));
//                    }else {
//                        Utility.showToast(SplashActivity.this, R.string.something_went_wrong);
//                    }
//                }
//
//            }
//
//            @Override
//            public void onFailure(Call<MasterDataModel> call, Throwable t) {
//                if (!SplashActivity.this.isFinishing()) {
//                    Utility.showToast(SplashActivity.this, R.string.something_went_wrong);
//                }
//            }
//        });
    }

}
