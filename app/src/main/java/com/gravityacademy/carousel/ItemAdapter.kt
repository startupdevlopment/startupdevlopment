package com.gravityacademy.carousel

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.gravityacademy.R
import com.gravityacademy.model.CategoryModel

class ItemAdapter(val categoryModels :ArrayList<CategoryModel>) : RecyclerView.Adapter<ItemViewHolder>() {

    private var items: List<CategoryModel> = listOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder =
        ItemViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_more_academy_book, parent, false))

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        holder.bind(items[position])
        holder.itemView.setOnClickListener {
            //itemClick(position,items[position])
        }
    }

    override fun getItemCount() = items.size

    fun setItems(newItems: List<CategoryModel>) {
        items = newItems
        notifyDataSetChanged()
    }
}

class ItemViewHolder(private val view: View) : RecyclerView.ViewHolder(view) {
    fun bind(item: CategoryModel) {
       // view.list_item_text.text = "${item.title}"
        val list_item_icon = view.findViewById<ImageView>(R.id.list_item_icon)
        list_item_icon.setImageResource(item.imageId)
    }
}