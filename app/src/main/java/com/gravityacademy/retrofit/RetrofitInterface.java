package com.gravityacademy.retrofit;

import com.gravityacademy.model.response.CommonResponse;
import com.gravityacademy.model.response.ExerciseLibraryModel;
import com.gravityacademy.model.response.ExercisesLibraryModel;
import com.gravityacademy.model.response.LoginResponse;
import com.gravityacademy.model.response.MasterDataModel;
import com.gravityacademy.model.response.SignUpModel;
import com.gravityacademy.model.response.WorkoutLibraryModel;
import com.gravityacademy.model.response.WorkoutOfTheDayModel;
import com.gravityacademy.utils.AppUtils;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.Response;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface RetrofitInterface {

    @POST("user/auth/signup")
    @FormUrlEncoded
    Call<SignUpModel> signUp(@Field("gender") String gender,
                             @Field("device_type") String device_type,
                             @Field("height") String height,
                             @Field("height_unit") String height_unit,
                             @Field("weight") String weight,
                             @Field("weight_unit") String weight_unit,
                             @Field("fn_level_id") String fn_level_id,
                             @Field("goals") String goals,
                             @Field("max_pullups") String max_pullups,
                             @Field("max_pushups") String max_pushups,
                             @Field("max_squats") String max_squats,
                             @Field("max_dips") String max_dips,
                             @Field("user_name") String user_name,
                             @Field("email") String email,
                             @Field("password") String password,
                             @Field("name") String name,
                             @Field("device_id") String device_id
    );

    @POST("user/auth/login")
    @FormUrlEncoded
    Call<LoginResponse> loginUser(@Field("device_type") String device_type,
                                  @Field("email") String email,
                                  @Field("password") String password,
                                  @Field("device_id") String device_id
    );

    @POST("user/auth/forgot_password")
    @FormUrlEncoded
    Call<CommonResponse> forgotPassword(@Field("email") String email);

    @POST("user/auth/resend_email_verification_code")

    @FormUrlEncoded
    Call<CommonResponse> verifyEmail(@Field("email") String email);

    @POST("user/auth/forgot_password_validate_code")
    @FormUrlEncoded
    Call<CommonResponse> validateOTP(@Field("key") String key
    );

    @POST("user/auth/change_password")
    @FormUrlEncoded
    Call<CommonResponse> changePassword(@Field("password") String password,
                                        @Field("key") String key
    );

    @POST("user/exercise/workout_detail")
    @FormUrlEncoded
    Call<ExercisesLibraryModel> getWorkoutDetails(@Field("workout_id") int workoutId,
                                                  @Field("fn_level_id") int fitnessLevelId
    );

    @POST("user/auth/logout")
    @FormUrlEncoded
    Call<CommonResponse> userLogout(@Field("sessionkey") String sessionkey
    );

    @POST("user/auth/resend_email_verification_code")
    @FormUrlEncoded
    Call<CommonResponse> resendEmailVerification(@Field("email") String email
    );

    @POST("user/auth/get_master_data")
    Call<MasterDataModel> getMasterData();


    @POST("user/exercise/exercise_library")
    Call<ExerciseLibraryModel> getExerciseLibraryData();

    @POST("user/exercise/workout_library")
    Call<WorkoutLibraryModel> getWorkoutLibrary();



    @POST("user/exercise/wo_of_the_day")
    Call<WorkoutOfTheDayModel> getWorkoutOfTheDay();

    @POST("")
    @Multipart
    Call<Response> createRecord(
            @Part("clinic_id") RequestBody clinicId,
            @Part("owners_first_name") RequestBody ownersFirstName,
            @Part("owners_last_name") RequestBody ownersLastName,
            @Part("owners_address") RequestBody ownersAddress,
            @Part("city") RequestBody city,
            @Part("state") RequestBody state,
            @Part("zip") RequestBody zip,
            @Part("pet_name") RequestBody pet_name,
            @Part("species") RequestBody species,
            @Part("weight") RequestBody weight,
            @Part("breed") RequestBody breed,
            @Part("gender") RequestBody gender,
            @Part("colour") RequestBody colour,
            @Part("dob") RequestBody dob,
            @Part("dod") RequestBody dod,
            @Part("personal_belongings") RequestBody personalBelongings,
            @Part("email") RequestBody email,
            @Part("service_type") RequestBody serviceType,
            @Part("cremation_note") RequestBody cremationNote,
            @Part("service_price") RequestBody servicePrice,
            @Part("urn") RequestBody urn,
            @Part("paw_print_price") RequestBody pawPrintPrice,
            @Part("engraving_price") RequestBody engravingPrice,
            @Part("customer_name") RequestBody customerName,
            @Part MultipartBody.Part customerSignature

    );
}
