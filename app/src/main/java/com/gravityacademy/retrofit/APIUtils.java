package com.gravityacademy.retrofit;


public class APIUtils {

    public static RetrofitInterface getAPIServiceJson() {
        return RetrofitClient.getClientJson().create(RetrofitInterface.class);
    }
}
