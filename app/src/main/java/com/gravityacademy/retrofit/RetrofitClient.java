package com.gravityacademy.retrofit;

import android.util.Log;

import com.gravityacademy.utils.AppConstant;
import com.gravityacademy.utils.AppUtils;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

import static com.gravityacademy.utils.AppConstant.SP_SESSION_KEY;

/**
 * @author csuryesh
 */

public class RetrofitClient {

    private static Retrofit retrofitJson = null;
    public static final String BASE_URL = "http://w3surface.com/gravity/";

    public static Retrofit getClientJson() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        // set your desired log level
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        // add your other interceptors …

        // add logging as last interceptor
        Log.e("Session-Key",DataHandler.getStringPreferences(SP_SESSION_KEY));
        httpClient.addInterceptor(logging);  // <-- this is the important line!
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Interceptor.Chain chain) throws IOException {
                Request original = chain.request();

                // Request customization: add request headers
                Request.Builder requestBuilder = original.newBuilder()
                        .addHeader(SP_SESSION_KEY, DataHandler.getStringPreferences(SP_SESSION_KEY))
                       // .addHeader(SP_SESSION_KEY, "d3a51702da9358b8f465a95e1414aebf")
                        // .addHeader("Device-Version", ""+Build.VERSION.SDK_INT)
                        // .addHeader("device_id", AppUtils.getAndroidId())
                        // .addHeader("Content-Type", "application/json")
                        // .addHeader("Device-Token", DataHandlerSharedPefrences.getStringPreferences(FIREBASE_TOKEN))
                        // .addHeader("Language", "en")
                        ;

                Request request = requestBuilder.build();
                return chain.proceed(request);
            }
        });
        httpClient.connectTimeout(60 * 1000, TimeUnit.SECONDS);
        httpClient.readTimeout(60 * 1000, TimeUnit.SECONDS);
        if (retrofitJson == null) {
            retrofitJson = new Retrofit
                    .Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(ScalarsConverterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(httpClient.build())
                    .build();
        }
        return retrofitJson;

    }
}
